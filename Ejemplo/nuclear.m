clear all;close all;
Elab=19; %en MeV
mp=9.012183; % valor encontrado en la tabla de isotopos
mt=63.929141; % valor encontrado en la tabla de isotopos
l=mp/mt;
J=2.9e27/1e-4; %valor dado en el ejercicio
anglab=[30 60 90 120 150];
anglab=(pi/180)*anglab; %angulo del laboratorio en radianes
N=[32983 2296 545 181 82];
seclab=N/J; %seccion eficaz del laboratorio en m^2
angCM=acos(-l.*sin(anglab).^2+cos(anglab).*sqrt(1-l^2*sin(anglab).^2)); % angulo del centro de masa en radianes
secCM=seclab.*(1+l*cos(angCM))./((1+l^2+2*l*cos(angCM)).^(3/2)); % seccion eficaz del centro de masa en m^2

e= 1.44e-15; %e^2 en MeV * m
Zp=4;
Zt=30;
Ecm=(mt/(mt+mp))*Elab;
a=(1/2)*(e*Zp*Zt/Ecm);
ruther=(a^2/4)*(1./(sin(angCM/2)).^4); %seccion eficaz de rutherford en m^2
%-----------Graficar
angCM=angCM*(180/pi); % radianes a grados
a^2;
secCM=secCM*1e28; %m^2 a barns
ruther=ruther*1e28; %m^2 a barns
figure(1);clf;
hold on
scatter(angCM,secCM)
scatter(angCM,ruther)
legend('CM','rutherford')
xlabel('\theta_{CM}(grados)');ylabel('d\sigma/d\Omega (b)');
hold off
print -djpeg -r100 figura1.jpeg
figure(2);clf; % ignorar el primer dato
hold on
scatter(angCM,secCM,'filled')
scatter(angCM,ruther,'filled')
legend('CM','rutherford')
xlabel('\theta_{CM}(grados)');ylabel('d\sigma/d\Omega (b)');
axis([50 160 0 0.75])
hold off
print -djpeg -r100 figura2.jpeg
figure(3)
X=secCM./ruther;

scatter(angCM,X,'filled')
xlabel('\theta_{CM}(grados)');ylabel('d\sigma_{CM}/d\Omega / d\sigma_{R}/d\Omega');
print -djpeg -r100 figura3.jpeg

